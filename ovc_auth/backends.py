from ovc_auth.models import AppUser
from django.contrib.auth.models import check_password
from django.contrib.auth.backends import ModelBackend

class OVCAuthenticationBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        user = None
        try:
            user = AppUser.objects.get(workforce_id=username)
        except AppUser.DoesNotExist:
            pass 
        except Exception as e:
            return None
        
        try:
            if not user:
                user = AppUser.objects.get(national_id=username)
        except AppUser.DoesNotExist:
            return None
        except Exception as e:
            return None
        if user and check_password(password, user.password):
                return user
        
        return None

    def get_user(self, user_id):
        try:
            if user_id:
                return AppUser.objects.get(pk=user_id)
        except AppUser.DoesNotExist:
            return None
        except Exception as e:
            pass
        return None