from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings
import requests
from rest_framework import status
import json

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'OVC_IMS.settings')
app = Celery('rest_client', broker='redis://localhost')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

from ovc_main.models import *
from ovc_main.core.rest.serializers import *
import datetime
from ovc_main.utils import lookup_field_dictionary as lookup_field

@app.task
def test(x, y):
    return x + y


@app.task
def upload():        
    server_url = 'http://127.0.0.1:8000/api/upload'
    if(AdminUploadForms.objects.filter(timestamp_uploaded=None) > 0):
        to_upload = AdminUploadForms.objects.filter(timestamp_uploaded = None)
        for record in to_upload:
            print ' A lot of forms to upload'
            print record.form_id,'record.form_id'
            if len(Forms.objects.filter(form_id = record.form_id))==0:
                continue
            form = Forms.objects.get(form_id = record.form_id)
            serializer = FormsSerializer(form)
            payload = {'data': serializer.data}
            print json.dumps(serializer.data),'serializer.data'
            #req = requests.post(server_url, data = payload)
            req = requests.post(server_url, auth=('admin', 'root'), data = json.dumps(serializer.data))
            print req.status_code,' == status.HTTP_201_CREATED(201)'
            if req.status_code == status.HTTP_201_CREATED:
                print 'upload successful'
                #update_upload_status(record.pk)
                record.timestamp_uploaded = datetime.datetime.now()
                record.save()
                print req.status_code,'status_code'
                print req.headers['content-type'],'content type'
                print req.text,'text'
            else:
                print 'upload NOT successful'
            
    
def update_upload_status(update_id):
    rec = AdminUploadForms.objects.get(form_id=update_id)
    rec.timestamp_uploaded = datetime.datetime.now()
    rec.save()

@app.task
def download(params):
    print 'downloading in task'
    section_to_download = params['section']
    list_codes_all = lookup_field.download_sections.keys()
    if section_to_download:
        from ovc_main.core.workers.capture_utililty import get_download_precedence
        
        sec_ids_precedence = get_download_precedence()
        if section_to_download == 'all':
            print 'DOWNLOAD: MULTIPLE section mode'
            count = 1
            while count <= len(sec_ids_precedence):
                download_section(sec_ids_precedence[count])
                count = count + 1
        else:
            print 'DOWNLOAD: SINGLE section mode'
            #TODO determine if any dependencies exist
            download_section(section_to_download)

def download_section(section_id):
    models_to_download = lookup_field.download_sections[section_id]['models']    
    ''' Logging fields '''
    capture_site_id = None #TODO
    num_of_records_success = 0
    num_of_records_fail = 0
    success = None
    if models_to_download:    
        print models_to_download,'models_to_download'
        #Initial logging
        m_download = update_download_status(m_download = None, capture_site_id = capture_site_id, section_id = section_id)
        for model_info in models_to_download:
            print model_info,'model_info'
            get_param = model_info['id']
            serialiser_str = model_info['serializer']
            server_url = 'http://127.0.0.1:8000/api/download'
            payload = {'type': get_param}
            #TODO security check for password
            req = requests.get(server_url, auth=('admin', 'root'), params = payload)
            object_list = []
            try:
                object_list = req.json()
            except Exception as e:
                print e
            if not object_list:
                continue
            print object_list,'object_list'
            serialiser_class = globals()[serialiser_str]
            for item in object_list:
                serializer = serialiser_class(data=item)
                #not sure why id is dropped from validated_data. so i will use the id from the json stream
                print item,'item'
                id = int(item['id'])
                if serializer.is_valid():
                    validated_data = serializer.validated_data
                    serializer.save(validated_data,id)
                    #TODO update logging table
                    num_of_records_success = num_of_records_success + 1
                    print 'save successful' 
                else:
                    #print item,'item'
                    print 'SAVE NOT SUCCESSFUL!.'
                    #TODO update logging table
                    num_of_records_fail = num_of_records_fail + 1
                    print serializer.errors
        #final logging
        update_download_status(m_download = m_download, num_of_records_success = num_of_records_success, num_of_records_fail = num_of_records_fail)

def update_download_status(m_download=None, capture_site_id=None, section_id=None, num_of_records_success=None, num_of_records_fail=None):
    if not m_download:
        m_download = AdminDownload(capture_site_id=capture_site_id, section_id=section_id, timestamp_started = datetime.datetime.now())
        m_download.save()
    else:
        if num_of_records_success > 0:
            m_temp = AdminDownload.objects.get(pk=m_download.pk)
            m_temp.timestamp_completed = datetime.datetime.now()
            m_temp.number_records = num_of_records_success
            m_temp.success = True
            m_temp.save()
        if num_of_records_fail > 0:
            m_download_fails = AdminDownload(
                                            capture_site_id=m_download.capture_site_id, 
                                            section_id=m_download.section_id, 
                                            timestamp_started = m_download.timestamp_started,
                                            timestamp_completed = datetime.datetime.now(),
                                            number_records = num_of_records_fail,
                                            success = False
                                            )
            m_download_fails.save()
        if num_of_records_success == 0 and num_of_records_fail == 0:
            m_temp = AdminDownload.objects.get(pk=m_download.pk)
            m_temp.delete()
            
    return m_download