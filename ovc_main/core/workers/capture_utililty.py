from ovc_main.models import AdminUploadForms, AdminDownload
from django.db.models import Count
from ovc_main.utils import lookup_field_dictionary as lookup_field,fields_list_provider as field_provider

def get_upload_queue():
    #queue = AdminUploadForms.objects.values('form_id').annotate(dcount=Count('form_id'))
    queue = AdminUploadForms.objects.filter(timestamp_uploaded=None)
    to_return = []
    for upload in queue:
        item = {'formtype': upload.form_id,
            'count': upload.form_id,}
        to_return.append(item)
    return to_return

def get_downloads_queue():
    print 'Get download queue'
    queue = AdminDownload.objects.all()
    to_return = []
    for downld in queue:
        started = '-'
        ended = '-'
        if downld.timestamp_started:
            started = downld.timestamp_started.isoformat()
        if downld.timestamp_completed:
            ended = downld.timestamp_completed.isoformat()
        item = {'section': field_provider.get_description_for_item_id(downld.section_id)[0],
            'started': started,
            'ended': ended,
            'num_of_recs': downld.number_records,
            'success': downld.success,}
        to_return.append(item)
    return to_return

def get_initial_data():
    has_pending_uploads = False
    has_pending_downloads = False
    uploads = []
    downloads = []
    if(AdminUploadForms.objects.filter(timestamp_uploaded=None) > 0):
        has_pending_uploads = True
        uploads = get_upload_queue()
    if(AdminDownload.objects.filter() > 0):
        has_pending_downloads = True
        downloads = get_downloads_queue()
    downld_sections = tuple([(k, v['title']) for k,v in lookup_field.download_sections.items()])
    downld_sections = sorted(downld_sections, key=lambda t: (t[1][0].isupper(), t[1]))
    
    initial_data = {'has_uploads':has_pending_uploads, 'has_downloads':has_pending_downloads, 'uploads':uploads, 'downloads':downloads, 'download_sections':downld_sections,}
    return initial_data

def get_download_precedence():
    downloads_prec = {}
    for section_id in lookup_field.download_sections.keys():
        if section_id == 'all':
            continue
        section_details = lookup_field.download_sections[section_id]
        downloads_prec[section_details['precedence']] = section_id
    return downloads_prec