from ovc_main.models import SetupList
from ovc_main.utils import lookup_field_dictionary
from ovc_main.models import SetupList, SetupGeorgraphy, RegOrgUnit, RegPerson, RegPersonsTypes, RegOrgUnitGeography, RegOrgUnitGdclsu, RegPersonsContact, RegPersonsAuditTrail
from django.db.models import Q
import operator 
from ovc_main.utils.geo_location import get_communities_in_ward
from ovc_main.utils.general import list_has_key
from datetime import date

initial_drop_item = [('','-----')]
alt_initial_drop_item = [('','All workforce/user types')]


#===========================Workforce Methods================================
def get_list_of_organisations(orgname,geo_ids_filter=[],user=None): 
    to_return = ''
    if geo_ids_filter:
        to_return = tuple([(l.unit_id, l.unit_name) for l in RegOrgUnit.objects.filter(unit_name__icontains=orgname, id__in=geo_ids_filter,is_void=False,date_closed=None)])
    else:
        if user:
            #print user.is_superuser,'user.is_superuser'
            if user.is_superuser:
                to_return = tuple([(l.unit_id, l.unit_name) for l in RegOrgUnit.objects.filter(unit_name__icontains=orgname,is_void=False,date_closed=None)])
    return to_return

def get_sex_list(sex=None):
    return tuple(initial_drop_item + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=lookup_field_dictionary.sex)])


def get_list_of_province_districts():
    optionstring = ''
    province_district = [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.dist)]
    
    for prov_dist in province_district:
        optionstring += '<option value="%s">%s</option>\n' % (prov_dist[0], prov_dist[1])   
    return optionstring

def get_Workforce_Type(search=None):
    workforce_type = None
    if search == None:
        workforce_type = initial_drop_item 
    else:
        workforce_type = alt_initial_drop_item
    return tuple(workforce_type + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category= lookup_field_dictionary.workforce_type)])

def get_Workforce_Type_as_list():
    all_wfc_types = []
    all_wfc_type_tpl=get_Workforce_Type(search=True)
    for kvp in all_wfc_type_tpl:
        if len(kvp) > 1:
            if kvp[0] == '':
                continue
            all_wfc_types.append(kvp[0])
    return all_wfc_types
        
def get_YesNo_Enums():
    choices = []
    return tuple(choices + [(l.the_order, l.item_description) for l in SetupList.objects.filter(item_category=lookup_field_dictionary.gen_YesNo)])

def get_yes_no_index(yes_no_value):
    return tuple([(l.the_order) for l in SetupList.objects.filter(item_category=lookup_field_dictionary.gen_YesNo, item_description = yes_no_value)])

def get_the_order_index_for_item(item_id, categoty):
    return tuple([(l.the_order) for l in SetupList.objects.filter(item_category=categoty, item_id = item_id)])

def get_description_for_item_id(item_id):
    return tuple([(l.item_description) for l in SetupList.objects.filter(item_id = item_id)])

def get_item_desc_for_order_and_category(the_order,category):
    return tuple([(l.item_description) for l in SetupList.objects.filter(the_order = the_order, item_category = category)])



#==============================End Workforce Methods===========================

def get_most_recent_record_date_for_person_id(person_id):
    recent_date = None
    persons = RegPerson.objects.filter(pk=person_id, is_void=False)
    reg_p = None
    for person in persons:
        reg_p = person
    if reg_p:
        audit_trails = RegPersonsAuditTrail.objects.filter(person_id=reg_p.pk,transaction_type_id=lookup_field_dictionary.web_interface_id)
        for audit_trail in audit_trails:
            if recent_date:
                if audit_trail.timestamp_modified:
                    if recent_date < audit_trail.timestamp_modified:
                        recent_date = audit_trail.timestamp_modified
            else:
                recent_date = geo.timestamp_modified
    return recent_date

def get_org_list():
     return tuple(initial_drop_item + [(l.pk, l.unit_name) for l in RegOrgUnit.objects.all()])

def get_setup_field_id_from_description(lookuptext):
    return SetupList.objects.get(item_description=lookuptext).item_id

def get_setup_field_description_from_item_id(lookup_item_id):
    item_description = None
    try:
         item_description = SetupList.objects.get(item_id=lookup_item_id).item_description
    except Exception as e:
        print e
        #print lookup_item_id, 'was not found in setulip database'
        #raise Exception('something went wroing query setuplist')
    return item_description


def get_list_of_organisation_types():
    org_drop_list = [('','All Types')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category='Organisational unit type')])

#===========================Beneficiary methods===================================
def adverse_conditions():
    adverse_conds = {l.item_id:l.item_description for l in SetupList.objects.filter(item_category=lookup_field_dictionary.adverse_condition)}
    return adverse_conds
def get_list_adverse_cond():
    adverse_cond = None
    choices = []
    try:
        adverse_cond = tuple(choices + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=lookup_field_dictionary.adverse_condition)])
    except Exception as e:
        print e
        raise Exception('Setup list query failed to load adverse conditions')
    return adverse_cond

def get_list_of_residentials(res_search_key):
    print 'loading residential institutions'
    res_institutions = None
    toreturn = {}
    
    try:
        res_institutions = RegOrgUnit.objects.filter(unit_name__icontains=res_search_key, unit_type_id = lookup_field_dictionary.org_unit_type_res_Institution, is_void = False)
        district_added = False
        ward_added = False
        cwac_added = False
        
        for res in res_institutions:
            
            org_area = RegOrgUnitGeography.objects.filter(organisation_id=res.pk, date_delinked=None)
            
            for org_a in org_area:
                if not district_added:
                    if ('district' == SetupGeorgraphy.objects.get(area_id=org_a.area_id).area_type_id):
                        if toreturn.has_key(res.pk):
                            toreturn[res.pk][1].append(org_a.area_id)
                            district_added = True
                        else:
                            toreturn[res.pk]=[res.unit_id + ' ' + res.unit_name,[res.pk]]
                            toreturn[res.pk][1].append(org_a.area_id)
                            district_added = True
            
            for org_a in org_area:
                if not ward_added:
                    if ('ward' == SetupGeorgraphy.objects.get(area_id=org_a.area_id).area_type_id): 
                        if toreturn.has_key(res.pk):
                            toreturn[res.pk][1].append(org_a.area_id)
                            ward_added = True
                        else:
                            toreturn[res.pk]=[res.unit_id + ' ' + res.unit_name,[res.pk]]
                            toreturn[res.pk][1].append(org_a.area_id)
                            ward_added = True
            
            glu = None
            gdclsu = RegOrgUnitGdclsu.objects.filter(organisation=res, is_void=False)
            for g in gdclsu:
                glu = g
            
            if glu:    
                org_reg = RegOrgUnit.objects.filter(pk=glu.gdclsu_id,is_void=False,is_gdclsu=True)
                            
                for org_a in org_reg:
                    if not cwac_added:
                        if org_a.is_active:
                            if toreturn.has_key(res.pk):
                                toreturn[res.pk][1].append(int(org_a.pk))
                                cwac_added = True
                            else:
                                toreturn[res.pk]=[res.unit_id + ' ' + res.unit_name,[res.pk]]
                                toreturn[res.pk][1].append(org_a.pk)
                                cwac_added = True
            
                    
    except Exception as e:
        print e
        raise Exception('Setup list query failed to load residential institutions')
    
    #print 'cwaki cwac', toreturn
    
    return toreturn

def get_location_from_unit_id(unit_id):
    #RegOrgUnitGdclsu.objects.create(organisation=new_org, gdclsu_id=community, date_linked=new_org.date_operational, date_delinked=new_org.date_closed)
    #RegOrgUnitGeography.objects.create(organisation=new_org, area_id=selected_location, date_linked=new_org.date_operational, date_delinked=new_org.date_closed)
    search_condition = []
    #token = beneficiary
    tuple(initial_drop_item + [(area_id, area_name) for location in SetupGeorgraphy.objects.filter(parent_area_id=area_id)])
    
    
def get_org_geo_from_id(org_id):
    return RegOrgUnitGeography.objects.filter(organisation=org_id)
    
def get_gdclsu_for_org(org_id):
    return RegOrgUnitGdclsu.objects.get(organisation=org_id)   

def get_org_from_id(res_id):
    return RegOrgUnit.objects.get(unit_id = res_id)

def get_item_desc_from_id(item_id):
    return SetupList.objects.get(item_id = item_id).item_description

def get_person_id(pers_id):
    return RegPerson.objects.get(beneficiary_id=pers_id, is_void=False).pk

def get_list_of_beneficiaries(beneficiary, person_type): 
    print 'loading guardians to control'   
    vals = None
    try: 
        search_condition = []
        #token = beneficiary
        search_condition.append(Q(first_name__icontains=beneficiary))
        search_condition.append(Q(surname__icontains=beneficiary))
        search_condition.append(Q(other_names__icontains=beneficiary))
        search_condition.append(Q(beneficiary_id__icontains=beneficiary))
        search_condition.append(Q(national_id__icontains=beneficiary)) 
        search_condition.append(Q(birth_reg_id__icontains=beneficiary)) 
        
        
        search_token = None
        if(person_type == 'guardian'):
            vals = tuple([(l.beneficiary_id,l.national_id, l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_guardian_guardian,date_of_death = None, is_void=False)]) 
        elif(person_type == 'workforce'):
            search_workforce = []
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=lookup_field_dictionary.workforce_type_ngo_id))
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=lookup_field_dictionary.workforce_type_gov_id))
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=lookup_field_dictionary.workforce_type_vol_id))
            
            vals = tuple([(l.workforce_id,l.national_id,l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_workforce),date_of_death = None, is_void=False)])
        else:
            return None
        
        #with this query, only persons of type guardian in the system, will show up in the look up control
        
    except Exception as e:
        print e
        raise Exception('Couldnt load beneficiaries')
    
    return vals

def get_guardian_contacts(grd_id):
    
    guard_contacts = {}
    
    reg_person = RegPerson.objects.filter(beneficiary_id = grd_id, is_void=False)
    g_person = None
    for reg_p in reg_person:
        g_person = reg_p
    
    if g_person:
        reg_contacts = RegPersonsContact.objects.filter(person=g_person,is_void=False)
        
        for reg_contact in reg_contacts:
            contact_type = SetupList.objects.get(item_id=reg_contact.contact_detail_type_id).item_description
            guard_contacts[contact_type] = reg_contact.contact_detail

    return guard_contacts

def get_person_from_wfc_id(wfc_id):
    if RegPerson.objects.filter(workforce_id=wfc_id, is_void=False).count()>0:
        reg_person = RegPerson.objects.get(workforce_id=wfc_id, is_void=False)
    
    return reg_person

def get_person_type(person_id):
    reg_person = RegPerson.objects.get(pk=person_id)
    pers_type = None
    if RegPersonsTypes.objects.filter(person=reg_person,is_void=False).count()>0:
        pers_type=RegPersonsTypes.objects.get(person=reg_person,is_void=False)
        if SetupList.objects.filter(item_id=pers_type.person_type_id).count()>0:
            setup_list = SetupList.objects.get(item_id=pers_type.person_type_id)
            pers_type = setup_list.item_description
    
    return pers_type

def get_beneficiaries(ben_token, person_type):
    vals = []
    
    try:
        search_condition = []
        #token = beneficiary
        search_condition.append(Q(first_name__icontains=ben_token))
        search_condition.append(Q(surname__icontains=ben_token))
        search_condition.append(Q(other_names__icontains=ben_token))
        search_condition.append(Q(beneficiary_id__icontains=ben_token))
        search_condition.append(Q(national_id__icontains=ben_token)) 
        search_condition.append(Q(birth_reg_id__icontains=ben_token))       
        
        search_token = None
        if(person_type == lookup_field_dictionary.child_guardian_guardian and ben_token):
            vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_guardian_guardian, is_void=False, date_of_death = None)
        elif(person_type == lookup_field_dictionary.child_beneficiary_ovc and ben_token):
            vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_beneficiary_ovc, is_void=False, date_of_death = None)
        elif ben_token and not person_type:
            vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), is_void=False, date_of_death = None)
        elif person_type and not ben_token:
            vals = RegPerson.objects.filter(regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_guardian_guardian, is_void=False, date_of_death = None)
        else:
            search_condition = []
            search_condition.append(Q(regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_guardian_guardian))
            search_condition.append(Q(regpersonstypes__person_type_id__contains=lookup_field_dictionary.child_beneficiary_ovc))
            vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), is_void=False, date_of_death = None)
    except Exception as e:
        print e
        raise Exception('Couldnt load beneficiaries')
    
    return vals       

def get_list_of_districts():
    toret = tuple([('','-----')] + [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.dist)])
    return  toret

def get_list_of_wards(districtid = None):
    to_return = None
    if districtid:
        to_return = [('','-----')]
        constituencies = SetupGeorgraphy.objects.filter(parent_area_id=districtid)
        if constituencies:
            for constituency in constituencies:
                wards = SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.ward,parent_area_id=constituency.area_id)
                if wards:
                    for ward in wards:
                        to_return.append((ward.area_id, constituency.area_name+'-%s' % ward.area_name))
    else:    
        to_return = tuple([('','-----')] + [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.ward)])
    
    #print districtid    
    #print 'to_return_ward',to_return
    return to_return

def get_list_of_cwacs(wards=None):
    to_return = None
    if wards:
        wards = [wardid for wardid, wardname in wards if wardid]
        communities = get_communities_in_ward(wards)
        communitiesfordisplay = [(com_id, '%s-%s' % (com_d_id, comm_name)) for com_id, com_d_id, comm_name in communities]
        return [('','-----')] + communitiesfordisplay
    else:
        to_return = tuple([('','-----')] + [(l.pk, l.unit_id +'-%s' % l.unit_name)for l in RegOrgUnit.objects.filter(is_gdclsu=True, is_void=False)])
       
    return to_return

def get_district_from_child_id(token_area_id):
    #print 'getting district list'
    not_district = True
    while not_district:
        if SetupGeorgraphy.objects.filter(area_id=token_area_id, area_type_id=lookup_field_dictionary.dist).count() > 0:
             if(SetupGeorgraphy.objects.get(area_id=token_area_id, area_type_id=lookup_field_dictionary.dist).area_type_id == 'district'):
                 not_district = False
                 tmp_area_id = SetupGeorgraphy.objects.get(area_id=token_area_id, area_type_id=lookup_field_dictionary.dist)
                 #print tmp_area_id.area_id
                 
                 if tmp_area_id.parent_area_id:
                     #print token_area_id
                     token_area_id = tmp_area_id.parent_area_id
                 else:
                     break
        else:
            break
    
    try:
        loc = SetupGeorgraphy.objects.get(area_id=token_area_id)
        
        return  SetupGeorgraphy.objects.get(area_id=loc.parent_area_id).area_name+'-%s' % loc.area_name  
    
    except Exception as e:
        print e
        raise Exception('Couldnt load location')

def get_list_of_ben_types():
    return get_list_from_types_category(lookup_field_dictionary.beneficiary_category)    
    
def get_list_from_types_category(item_type):
    org_drop_list = [('','All Types')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=item_type)])
        

def get_list_children_from_parent_area_id(area_id):
    consti = []
    toreturn = initial_drop_item
    try:
        consti = SetupGeorgraphy.objects.filter(parent_area_id=area_id, area_type_id = 'constituency' )#tuple(initial_drop_item + [(location.area_id, location.area_name) for location in SetupGeorgraphy.objects.filter(parent_area_id=area_id)])
        
        if consti.count() > 0:
            for cons in consti:
                toreturn.append([(location.area_id, ('%s - %s' + cons.area_name, location.area_name)) for location in SetupGeorgraphy.objects.filter(parent_area_id=cons.area_id, area_type_id = 'ward')])
        
    except Exception as e:
        print e
        raise Exception('Loading list of wards failed')
 
    return toreturn

def province_district_control_list():
    optionstring = ''
    province_district = [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id='district')]
    
    for prov_dist in province_district:
        optionstring += '<option value="%s">%s</option>\n' % (prov_dist[0], prov_dist[1])
        
    return optionstring


def build_geo_control_option_list(selected_location, location, parent, setdisabled=False):
    optionstring = ''
    #print selected_location, 'we are suppose to select this'
    #print location.area_id
    if selected_location and location.area_id in selected_location:
        #print 'we are in here', selected_location
        optionstring = '<option value="%s" selected>%s-%s</option>\n' % (location.area_id, parent, location.area_name)
    else:
        if setdisabled and selected_location:
            optionstring = '<option value="%s" disabled>%s-%s</option>\n' % (location.area_id, parent, location.area_name)
        else:
            optionstring = '<option value="%s">%s-%s</option>\n' % (location.area_id, parent, location.area_name)
    return optionstring

def geo_control_select_list(geotype, selected_location=None, parentfilter=None, grandparentfilter=None,setdisabled=True):
    #print selected_location, 'this is our selected location'
    optionstring = ''
    geolisthasparent = False
    if SetupGeorgraphy.objects.filter(area_type_id=geotype).count() > 0:
        geolisthasparent = True
        
    if parentfilter:
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype, parent_area_id__in=parentfilter):
            parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
    elif grandparentfilter:
        parents =  [parent.area_id for parent in SetupGeorgraphy.objects.filter(parent_area_id__in=grandparentfilter)]
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype, parent_area_id__in=parents):
            parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
    else:
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype):
            parent = ''
            if geolisthasparent:
                parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
        
    return optionstring

def community_control_populate(selected_community=None, parent_filter=None):
    optionstring = ''
    if parent_filter:
        for communityid, communityorgid, communityname in get_communities_in_ward(parent_filter):
            if selected_community and communityid in selected_community:
                optionstring += '<option value="%s" selected>%s %s</option>\n' % (communityid, communityorgid, communityname)
            else:
                optionstring += '<option value="%s">%s %s</option>\n' % (communityid, communityorgid, communityname)
                '''if selected_community:
                    optionstring += '<option value="%s" disabled>%s %s</option>\n' % (communityid, communityorgid, communityname)
                else:
                    optionstring += '<option value="%s">%s %s</option>\n' % (communityid, communityorgid, communityname)'''
    return optionstring

def districts():
    districts = [(dst.area_id, dst.area_name) for dst in SetupGeorgraphy.objects.filter(area_type_id='district')]
    return districts


def get_related_organisation(organisation_id):
    
    rel_orgs = RegOrgUnit.objects.filter(parent_org_unit_id=organisation_id).values_list('pk', flat=True)
    if not rel_orgs:
        return [organisation_id]
    return list(rel_orgs).append(organisation_id)

def get_user_related_organisation(user, exclude_orgs=[]):
    related_orgs = []
    
    if user.is_superuser:
        related_orgs = [(org.pk, org.unit_name)for org in RegOrgUnit.objects.filter(is_void=False).exclude(pk__in=exclude_orgs) if org.is_active]
        return initial_drop_item + related_orgs
    
    from ovc_auth.models import OVCUserRoleGeoOrg
    orgs = [orgrole.org_unit.pk for orgrole in OVCUserRoleGeoOrg.objects.filter(user=user) if orgrole.org_unit.is_active]
    
    if not orgs:
        orgs = []
    user_orgs = [(org_role.org_unit.pk, org_role.org_unit.unit_name) for org_role in OVCUserRoleGeoOrg.objects.filter(user=user) if org_role.org_unit.is_active]
    related_orgs = [(org.pk, org.unit_name) for org in RegOrgUnit.objects.filter(parent_org_unit_id__in=orgs, is_void=False).exclude(pk__in=exclude_orgs) if org.is_active]
    return initial_drop_item + list(set(user_orgs + related_orgs))

def calculate_age(birth_date):
    '''Adapted from http://stackoverflow.com/questions/2217488/age-from-birthdate-in-python'''
    tmp = birth_date
    if tmp:
        today = date.today()
        try: 
            birthday = tmp.replace(year=today.year)
        except ValueError: 
            birthday = tmp.replace(year=today.year, month=birth_date.month+1, day=1)
        if birthday > today:
            return today.year - tmp.year - 1
        else:
            return today.year - tmp.year
    return -1